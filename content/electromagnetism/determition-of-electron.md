+++
title = "Экспериментальное определение заряда электрона"
weight = 20

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++

### 20\. Экспериментальное определение заряда электрона (опыт Милликена).

![miliken experience](https://raw.re128.net/kogeletey/knw-media/electromagnetism/miliken-experience.png)

Заряд электрона был определен с большой точностью Милликеном. В закрытое пространство между пластинами конденсатора вводилось масло.
При разбрызгивании капельки электризовались их можно было установить неподвижно. 
Равновесие наступает при

$$
P'=e' E
$$

- $P'$ - результирующая сила тяжести и архимедовой силы $4/3\pi r^3(\rho-rho_0)g
- $\rho$ - плотность капельки
- $\rho_0$ - плотность воздуха

Для того чтобы найти $e'$ нужно определить $r$, который вычисляется из скорости равномерного падения.

$$
v_0=\frac{2r^2(\rho-rho_0)g}{9\eta}
$$

Движение капельки наблюдалось с помощью микроскопа, 
определялось время за которое она проходила между двумя нитями.

Зафиксировать равновесие капельки очень трудно. 
Поэтому включается такое поле, под действием которого капелька двигалась вверх.

{% katex(block=true) %}
$$
P'+6\pi\eta r v_{E}=e' E \\
e' = 9\pi\sqrt{\frac{2\eta^3 v_0}{(\rho-\rho_0)g}}\frac{v_0+v_E}{E}
$$
{% end %}

Измерив скорость $v_E$, Милликен вызывал ионизацию воздуха. 
Отдельные ионы прилипали к капельке, в результате которого скорость менялась.
А вот заряд всегда были кратными, тем самым доказав дискретность заряда.
$$
e=1.60\cdot 10^{-19}k=4.80\cdot 10^{-10}
$$

