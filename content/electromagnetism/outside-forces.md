+++
title = "Сторонние силы. Электродвижущая сила"
weight = 17

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++


### 17\. Изменение потенциала вдоль проводника с током. Сторонние силы. Электродвижущая сила (ЭДС). Закон Ома для участка цепи, содержащего ЭДС, и для замкнутой цепи.

## Сторонние силы

При наличие лишь кулоновских сил, стационарное поле становится статическим.
Так как положительные носители перемещались бы из мест с большим потенциалом к меньшим, а отрицательные в обратном направлении.
И это ведет к выравниванию потенциала.

Чтобы этого не происходило, в цепи постоянного тока, перенос заряда происходит против сил электрического поля, в сторону возрастания потенциала положительных зарядов $\varphi$
Перенос возможен с помощью сил не электростатического происхождения - сторонних.
Они необходимы как на отдельных участках цепи, либо во всей цепи.
Физическая природа их различна.

## Обобщенный закон Ома
Для количественной характеристики, вводят понятия поле сторонних сил и его напряженность $\vec{E}^*$
Численно равен сторонней силе, действующий на положительный заряд.

Если под действием электрического поля возникает ток плотности $\vec{j}=\sigma\vec{E}$, то под совместным действием обоих полей 
плотность равна

$$
\vec{j}=\sigma(\vec{E}+\vec{E^*})
$$

Это выражение является обобщенным законом Ома в локальной форме

## Закон ома для неоднородного участка цепи
тот участок, на котором действуют сторонние силы

В случае тонких проводов $j$ будет одинакова во всех сечениях провода.
Площадь сечения $S$.

Разделим уравнение на $\sigma$, умножим на элемент оси провода $dl$, взятый по направлению от сечения 1 до 2.
$$
\int_{1}^{2}{\frac{j dl}{\sigma}}=\int_{1}^{2}{E dl}+\int_{1}^{2}{E^*dl}
$$

Возьмем проекцию $\vec{j}$, направление зависит от $dl$, если $\vec{j} \upuparrows dl$, то $j\gt 0$, в противном случае
$j\lt 0$.
Сила тока одинакова, поэтому выносим ее за знак интеграла.

$$
\int_{1}^{2}{\frac{j dl}{\sigma}}=I\int_{1}^{2}{\rho\frac{dl}{S}}
$$

Интеграл от второй части является полным сопротивлением между сечением 1 и 2

## ЭДС

В правой части получаем разность потенциалов, $\varphi_1-\varphi_2$, а второй представляет собой
**электродвижущую силу**(эдс) $\varepsilon$, действующем на участке

$$
\varepsilon_{12} = \int_{1}^{2}{E^*dl}
$$

Величина способствует движению положительных носителей в выбранном направление, если $\varepsilon_{12}\gt 0$,
препятствует $\varepsilon_{12}\lt 0$

После преобразований выше, уравнение имеет вид
$$
R I = \varphi_1-\varphi_2+\varepsilon_{12}
$$

и  выражает **интегральную форму закона Ома для неоднородного участка цепи**

![general law ohm](https://raw.re128.net/kogeletey/knw-media/electromagnetism/general-law-ohm.png)

Если $\varphi_1=\varphi_2$, то уравнение принимает вид

$$
R I = \varepsilon
$$

- R - полное сопротивление замкнутой цепи
- $\varepsilon$ - сумма отдельных ЭДС

При размыкание источника, $\varepsilon$ на клеммах будет равен $\varepsilon=\varphi_1-\varphi_2,I=0$

