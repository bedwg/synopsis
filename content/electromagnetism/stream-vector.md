+++
title = "Поток вектора"
weight = 3

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++

### 3.	Поток вектора напряженности. Теорема Гаусса для вектора напряженности электростатического поля. Дифференциальная форма теоремы Гаусса.

## Поток вектора напряженности

Если имеется некоторая произвольная поверхность $S$, то поток вектора $\vec{E}$ через нее

$$
\Phi = \oint_{S}^{}{\vec{E}d\vec{S}}
$$

Эта величина зависит не только от конфигурации поля $\vec{E}$, но и от выбора нормали.
Если поверхность замкнутая, то рекомендуется выбирать внешнюю нормаль(наружу области)

## Теорема Гаусса

Поток вектора $\vec{E}$ сквозь произвольную поверхность зависит только от суммы зарядов,
охватываемых этой поверхностью.

$$
\oint_{}^{}{\vec{E}d\vec{S}} = \frac{q_{int}}{\varepsilon_{0}}
$$

### Доказательство
Рассмотрим поле одиночного заряда $q$ и окружим его произвольной замкнутой поверхностью $S$.
Найдем поток вектора $\vec{E}$ сквозь элемента $d\vec{S}$

$$
d\vec{\Phi}=\vec{E}d\vec{S}=E dS\cos{\alpha}=\frac{1}{4\pi\varepsilon_0}dS\cos{\alpha}=\frac{q}{4\pi\varepsilon_0}d\Omega
$$

![solid angle](https://raw.re128.net/kogeletey/knw-media/electromagnetism/solid-angle.jpg)

$d\Omega$ - телесный угол, опирающийся на элемент поверхности $dS$, с вершиной расположение заряда $q$
Интегрирование по вей поверхности будет эквивалентно интегрированию по телесному углу, поэтому мы получим формулу теоремы гаусса.

При более сложном случае телесный угол может принят как положительные, так и отрицательные значения.
Если $q$ расположен вне замкнутой поверхности $S$, то поток вектора $E$ равен нулю.
Если провести касательную плоскость, так чтобы она касалась $S$, тогда интегрирование будет эквивалентно интегрированию по $\Sigma$, внешняя сторона поверхности $S$ будет видна под углом $\gt 0$, а внутренняя под телесным углом.
В сумме получаем 0, $\Phi=0$.
Следовательно сколько линий входит в обьем, столько и **выходит**.

## Система точечных зарядов

Если система состоит из точечных зарядов, то согласно принципу суперпозиции, поток вектора можно записать

$$
\oint{\vec{E}d\vec{S}} =\sum{\oint{\vec{E_k}d\vec{S}}} = \sum{\Phi_k}
$$

Каждый интеграл в правой части равен $q_i/\varepsilon_0$ если заряд находится внутри замкнутой поверхности, и нулю если снаружи. 

Учтем случай, когда заряды распределены непрерывно с обьемной плотностью, зависящей от координат.
Тогда в правой части
$$
q_{int}=\int{\rho dV}
$$
интегрирование проводится только по обьему внутри $S$.

Поток вектора, в отличие от самого поля $\vec{E}$, зависит только от внутренней поверхности. 
То есть если мы передвинем заряды, без пересечения $S$, то поток вектору будет прежний, а вот поле $E$ изменится.

## Вывод в дифференциальной форме

Теорема Гаусса-Остроградского для вектора $\vec{A}$

{% katex(block=true) %}
$$
\int{\vec{A}d\vec{S}}=\int{\nabla\cdot\vec{A}}\implies \\
\oint{\vec{E}d\vec{S}} = \int{\nabla\cdot\vec{E}} \\ 
q=\int{\rho dV}\implies \int{\nabla\cdot\vec{E}}=1/\varepsilon_0\int{\rho dV}
$$
{% end %}

После дифференцирования

$$
\nabla\cdot\vec{E}=\frac{\rho}{\varepsilon_0}
$$
