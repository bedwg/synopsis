+++
title = "Собственные значения и собственные функции"
weight = 9

[extra]
math = true
+++

Если ядро однородного интегрального уравнения Фредгольма вырожденное, то
задача о нахождении собственных чисел и собственных функций
интегрального уравнения сводится к поиску собственных значений некоторой
матрицы. Действительно, всякое решение однородного уравнения имеет вид

$$
y(x) = \lambda \sum\limits_{k=1}^n c_k p_k(x)
$$ 

где числа $c_k$ являются решениями однородной системы

$$
c_k - \lambda \sum\limits_{i=1}^n a_{ki} c_i = 0
$$

Эта система может быть переписана в матричной форме 

$$
(I - \lambda A) \textbf{C} = 0 ~ \text{или} ~ (A - \mu I) \textbf{C} = 0, ~ \lambda, \mu \neq 0
$$

где $I$ - единичная матрица, $A = (a_{i j}), ~ \textbf{C}$ - столбец, состоящий из чисел $c_i$ 
Таким образом, собственные числа интегрального уравнения совпадают с отличными от нуля собственными числами матрицы $A$ и могут быть найдены из уравнения 
$$
\det(A - \mu I) = 0
$$

## Найти собственные числа и собственные функции интегрального уравнения

$$y(x) = \lambda \int\limits_0^1 (x t - 2 x^2) y(t) dt.$$

*Решение.* Ядро $K(x,t) = x t - 2 x^2$ --- вырожденное:
$$p_1(x) = x, \quad p_2 (x) = -2 x^2, \quad q_1(t) = t, \quad q_2(t) = 1.$$
Найдем компоненты матрицы $A:$
$$a_{11} = \int\limits_0^1 s^2 ds = \dfrac{1}{3}, \quad a_{12} =-2 \int\limits_0^1 s^3 ds = -\dfrac{1}{2} \\\\
a_{21} = \int\limits_0^1 s ds = \dfrac{1}{2}, \quad a_{22} = -2 \int\limits_0^1 s^2 ds = -\dfrac{2}{3}$$
Уравнение для нахождения собственных значений примет вид:
$$\det(A-\mu I) = \begin{vmatrix}
    ~\dfrac{1}{3} - \mu & -\dfrac{1}{2} \\\\
    \dfrac{1}{2} & -\dfrac{2}{3}-\mu~\end{vmatrix} =
\left(\mu + \dfrac{1}{6}\right)^2 = 0
$$

Следовательно, интегральное уравнение имеет только одно собственное
значение $\mu = -\dfrac{1}{6}$ (характеристическое число
$\lambda = -6$). Соответствующий ему собственный вектор находим решая
систему:

$$
\begin{pmatrix}
    \dfrac{1}{2} & -\dfrac{1}{2} \\\\
    \dfrac{1}{2} & -\dfrac{1}{2} 
\end{pmatrix}
\begin{pmatrix}
    c_1 \\\\
    c_2
\end{pmatrix}
= \begin{pmatrix}
    0 \\\\
    0
\end{pmatrix}
$$
откуда находим $c_1 = c_2 = C,$ где $C$ - произвольная константа. 
Согласно формуле собственной функцией интегрального уравнения будет
$$
y(x) = - 6\,(c_1 x - 2\,c_2 x^2) = C x\,(1 - 2x)
$$

## Исследовать решения интегрального уравнения

$$y(x) = \lambda \int\limits_{-\pi}^{\pi} (x^2 \cos t - x \sin t) y(t) dt + \cos x$$
в зависимости от значений параметра $\lambda.$

*Решение.* Поскольку ядро интегрального уравнения
$K(x, t) = x^2 \cos t - x \sin t$ вырожденное, то его решение можно
свести к решению алгебраической системы, которая может быть
записана в матричной форме: 
$$
  (I - \lambda A)\,\textbf{C} = \textbf{B}
$$ 
где $\textbf{B}$ - столбец из коэффициентов $b_i$.

Вычислим коэффициенты матрицы $A$ и столбца $B$

$$
p_1 (x) = x^2, \quad p_2 (x) = x,  \quad q_1 (t) = \cos t, \quad q_2(t) = \sin t
$$

$$
\begin{array}{llll}
        a_{11} = \int\limits_{-\pi}^{\pi} \cos s s^2 ds = - 4 \pi, & \quad a_{12} = \int\limits_{-\pi}^{\pi} \cos s s ds = 0\\\\
        a_{21} = \int\limits_{-\pi}^{\pi} \sin s s^2 ds = 0, & \quad a_{22} = \int \limits_{-\pi}^{\pi} \sin s s ds = -2 \pi\\\\
        b_1 = \int\limits_{-\pi}^{\pi} \cos^2 sds = \pi, & \quad b_2 = \int\limits_{-\pi}^{\pi} \sin s \cos s ds = 0.
    \end{array}
$$

Система имеет вид

$$
\begin{pmatrix}
    1 + 4 \pi \lambda & 0 \\\\
    0 & 1 + 2 \pi \lambda 
\end{pmatrix}
\begin{pmatrix}
    c_1 \\\\
    c_2
\end{pmatrix}
= \begin{pmatrix}
    \pi \\\\
    0
\end{pmatrix} \\\\
\det (I - \lambda A) = (1 + 4 \pi \lambda) (1 + 2 \pi \lambda) = 0, ~ \text{когда} ~ \lambda = -\dfrac{1}{4 \pi} ~ \text{или} ~ \lambda = -\dfrac{1}{2 \pi}
$$

При любых $\lambda \neq -\dfrac{1}{2 \pi}, ~ -\dfrac{1}{4 \pi}$ система имеет единственное решение 
$$
c_1 = \dfrac{\pi}{1 - 4 \pi \lambda}, ~ c_2 = 0
$$
В этом случае решением интегрального уравнения будет функция

$$
y(x) = \cos x + \dfrac{\lambda \pi}{1- 4 \pi \lambda} x^2
$$

Если $\lambda = -\dfrac{1}{4 \pi}$ то система
$$\begin{pmatrix}
    0 & 0 \\\\
    0 & 1/2
\end{pmatrix}
\begin{pmatrix}
    c_1 \\\\
    c_2
\end{pmatrix} = 
\begin{pmatrix}
    \pi \\\\
    0
\end{pmatrix}
$$ 
решений не имеет. 
Следовательно, при данном значении $\lambda$ не имеет решений и интегральное уравнение.

При $\lambda = -\dfrac{1}{2 \pi}$ решением системы 
$$
\begin{pmatrix}
    -1 & 0 \\\\
    0 & 0
\end{pmatrix}
\begin{pmatrix}
    c_1 \\\\
    c_2
\end{pmatrix} = 
\begin{pmatrix}
    \pi \\\\
    0
\end{pmatrix}
$$ 
будет $c_1 =-\pi, ~ c_2 = C$ и соответствующее решение
интегрального уравнения:
$$
y(x) = \cos x + \lambda (c_1 x^2 + c_2 x) = \cos x + x^2/2 + C x
$$

## Задания

Найти собственные значения и собственные функции интегрального
уравнения.

$$\begin{array}{ll}
        \textbf{2.1} ~ y(x) = \lambda \int\limits_0^1 (1 + 2x) y(t) dt &
        \quad \quad \quad \quad \textbf{2.2} ~ y(x) = \lambda \int\limits_0^1 (1-x^2) y(t) dt \\\\
        \textbf{2.3} ~ y(x) = \lambda \int\limits_0^{\pi} x \sin t y(t) dt &
        \quad \quad \quad \quad \textbf{2.4} ~ y(x) = \lambda \int\limits_0^{\pi}\cos x \cos t y(t) dt \\\\
        \textbf{2.5} ~ y(x) = \lambda \int\limits_0^{\pi} \sin (x+t) y(t) dt &
        \quad \quad \quad \quad \textbf{2.6} ~ y(x) = \lambda\int\limits_0^{\pi} \cos (x-t) y(t) dt
    \end{array}$$

Исследовать решения интегральных ур-й при различных значениях параметра
$\lambda.$

$$\begin{array}{ll}
        \textbf{3.1} ~ y(x) = \lambda \int\limits_0^1 (1 + 2x) y(t) dt + 1 - \dfrac{3}{2} x &  \quad \textbf{3.2} ~ y(x) = \lambda \int\limits_0^1 x y(t) dt + \sin 2 \pi x \\\\
        \textbf{3.3} ~ y(x) = \lambda \int\limits_0^{\pi} \sin x \cos t y(t) dt + \cos x & \quad \textbf{3.4} ~ y(x) = \lambda \int\limits_0^1 x \,sin 2 \pi t y(t) dt + x \\\\
        \textbf{3.5} ~ y(x) = \lambda \int\limits_{-1}^1 (1 + x t) y(t) dt + \sin \pi x & \quad \textbf{3.6} ~ y(x) = \lambda \int\limits_0^{\pi} \cos (x+t) y(t) dt + 1.
    \end{array}$$
