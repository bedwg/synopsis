+++
title = "Потенциальные силы и энергия"
weight = 5

[taxonomies]
authors = ["kogeletey","bedwg"]

[extra]
math = true
+++

$$
\int_{\Gamma_1}F^(p)dr = \int_{\Gamma_2}F^(p)dr
$$

Условие что работа не зависит от формы траектории
$$
\oint_{\Gamma}{F dr}=0 \\\
\nabla\times F = 0
$$

$$
F = \nabla \Phi
$$

Потенциальная энергия

$$
F = - \nabla U(r,t)
$$

Работа потенциальных сил

$$
\delta A = dr F = (-dr \cdot \nabla U)
$$

Гироскопические силы
$$
F=\kappa[\vec{v}\times A]
$$
не совершают работу

$$
E = T+U
$$

Если нет диссипативных сил то энергия сохраняется.

Если в системе есть симметрии, каждой из них соответствуют сохраняющиеся величина.
