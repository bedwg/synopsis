+++
title = "Аналитическая механика. Механика Лагранжа. Принцип наименьшего действия. Уравнения Лагранжа-Эйлера"
weight = 18
+++

## 18. Функция Лагранжа. Механическое действие. Принцип наименьшего действия.

Посчитаем
$$
L = \int_{\Gamma}{n(r)dl}\to min
$$

Аналогичными свойствами предполагает действие
$$
S = \int_{t_1}^{t_2}{L(q,\dot{q},t)dt} = S[q(t)]
$$
> функционал

$$
S= S_{min}{[q^{*}(t)]}
$$

Функция Лагранжа
$$
L = T - U \\\\
J [\text{Erg}]
$$

Размерность действия
$$
[S] = [J]\cdot c
$$
> такая же постоянная планка $(\hbar)$

1. Работает класcическая механика если $[S] \gg \hbar$
2. Квантовая механика $[S] \sim \hbar$

Интеграл по путям, если по всем законам движения в квантовом мире, формулировка Ричарда Феймана

##  19. Уравнение Лагранжа-Эйлера для системы материальных точек при учете диссипации. Связь уравнений Лагранжа-Эйлера и II закона Ньютона.

Посмотреть вариационное исчисление(задача о брохистохроме).
Ищем с закрепленной концами
$$
a(t_1) = q_1, a(t_1) = a_2 \\\\
\partial S = 0
$$

Можно разложить в ряд Тейлора и после вычислений получим
$$
\frac{d}{dt}(\frac{\partial l}{\partial{\dot{q_i}}}-\frac{\partial l}{\partial q_i}) = 0 
$$

Для одномерного случая
$$
L = \frac{m}{2}\dot{q^2}-U(q,t) \\\\
\frac{\partial L}{\partial \dot{q}}=m\dot{q}\\\\
m\ddot{q}= - \frac{\partial U}{\partial q}
$$

### С учетом диссипации

Получим систему уравнений подобных выше для $(x,y,z)$
$$
\frac{d}{dt}(\frac{\partial l}{\partial{\dot{\vec{r_i}}}}-\frac{\partial l}{\partial \vec{r_i}}) = 0 
$$

Тогда с силами
$$
m\ddot{q}= - \frac{\partial U}{\partial q} + \vec{F^d}
$$

Связь Релея
$$
D = \frac{k}{r}\vec{\dot{r}}
$$

$$
\frac{d}{dt}(\frac{\partial l}{\partial{\dot{\vec{r_i}}}})-\frac{\partial l}{\partial \vec{r_i}} - \frac{\partial D}{\partial \dot{q}} = 0 
$$


