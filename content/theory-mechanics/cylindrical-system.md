+++
title = "Цилиндрическая система координат"
weight = 2

[extra]
math = true
+++

2. Кинематика материальной точки. Радиус-вектор, скорость и ускорение в цилиндрической и естественной системах координат.

$$
(\rho,\varphi,z),\{\vec{n}_p,\vec{n}_{\varphi},\vec{n_z}\}
$$

## Радиус

$$
\vec{r}=\rho n_{\rho}+\rho n_{\varphi}+\rho n_{z}
$$

## Скорость
$$
\vec{v}=\dot{\rho}n_{\varphi}+\rho\dot{n_{\varphi}}+\dot{z}n_{z}+z \dot{n_{z}}
$$

или

$$
\vec{\dot{\rho}}=(\dot{\rho},\rho\dot{\varphi},\dot{z})
$$

## Ускорение
$$
w = (\ddot{\rho}-\dot{\rho\varphi},2\ddot{\rho}+\rho\dot{\varphi}\ddot{\varphi},\ddot{z})
$$


## Матрица поворота

$$
\begin{pmatrix}
\vec{n_{\rho}} \\\\
\vec{n_{\varphi}}
\end{pmatrix}=
\begin{pmatrix}
\cos{\varphi} & \sin{\varphi} \\\\
\sin{\varphi} & \cos{\varphi} 
\end{pmatrix}
\begin{pmatrix}
\vec{n_x} \\\\
\vec{n_y}
\end{pmatrix}
$$

# Сферическая система

- $|\vec{r}| = r$
- $(\vec{r},\vec{n_z})=\theta$ - полярный угол(наклонение)

$$
\theta\in [0,\pi]
$$

- $\varphi \in [0,2\pi]$ - азимутальный угол

## Координатные поверхности и линии

изменение при зафиксированной координате дадут поверхность

- например конус с углом $\theta$ при фиксированной $\theta$

координатные линии - фиксируем две координаты, и получаем третью

$$
\vec{r}=(r,0,0)
$$

$$
\vec{n_r}=(\cos{\varphi},\sin{\varphi}\sin{\theta},\cos{\theta})\\\\
\vec{n_{\theta}}=(\cos{\varphi}\cos{\theta},\sin{\varphi}\cos{\theta},-\sin{\theta})\\\\
\vec{n_\varphi}=(-\sin{\varphi},\cos{\phi})\\\\
$$

Скорость

$$
\dot{n}_{r}=(-\dot{\theta},\sin{\theta}\dot{\varphi}) \\\\
\dot{n}_{\theta}=(-\dot{\theta},\cos{\theta}\dot{\varphi}) \\\\
\dot{n}_{\varphi}=(-\dot{\varphi}\sin{\theta},-\dot{\varphi}\cos{\theta})
$$
